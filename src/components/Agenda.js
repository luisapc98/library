import React , {Component} from 'react'
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.min.css';
import Avatar from '@material-ui/core/Avatar';
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import pdf from '../img/pdf.png';
import arrow from '../img/arrow.png';
import { DatePicker } from 'antd';
import {NavLink} from 'react-router-dom'
import { Checkbox, Row, Col } from 'antd';
import ProyectorPlus from '../img/ProyectorPlus.png';
import axios from 'axios'
import moment from 'moment'
import { Alert } from 'antd';
import { Table } from 'antd';
import { Popconfirm, message , Modal } from 'antd';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;


function deleteSucess (){
    let context = this
    Modal.success({
        title: 'Orden eliminado',
        content: '',
        onOk() {
            window.location.reload()
          }, 
      });
}


const columns = [{
    title: 'Nombre',
    dataIndex: 'name',
}, {
    title: 'proyector',
    dataIndex: 'nameProyector',
}, {
    title: 'Hora initial',
    dataIndex: 'initialHour',
},
    {
        title: 'Hora final',
        dataIndex: 'finalHour',
    },
    {
        title: 'Lugar',
        dataIndex: 'place',
    }
    ,{
        title: 'Funciones',
        dataIndex: 'id',
        key: 'id',
        render: text =>  <Popconfirm title="Estas seguro de eliminar este orden?" onConfirm={()=>{
            axios.delete('http://localhost:8000/orderProyector/'+ text)
            deleteSucess()
        }} okText="Eliminar" cancelText="No"><a href="#">Delete</a></Popconfirm>,
      },
];



function rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  
  function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();
  

    function onChange(checkedValues) {
      console.log('checked = ', checkedValues);
    }

    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }
  
  
  function onChange(date, dateString) {
    console.log(date, dateString);
  }


  const styles = theme => ({
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
    },
  });


     
class Agenda extends Component {
    constructor(){
        super()
        this.state = {
            visible: false,
            fromDate : '',
            toDate : '',
            showErrorPdf : false,
            messageError : '',
            date : '',
            data : []
        }
    }

    showModal = () => {
      this.setState({
        visible: true,
      });
    }


    handleOk = (e) => {
      console.log(e);
      this.setState({
        visible: false,
      });
    }

    handleCancel = (e) => {
      console.log(e);
      this.setState({
        visible: false,
      });
    }

    componentDidMount(){
        moment.locale('en')
        let date = moment(Date()).format("LLLL")
        console.log(date)
        this.setState({
            date
        })


        axios.get('http://localhost:8000/orderProyector').then(response=>{
            let arrayOfOrders = []
            if(response.status === 200){
                response.data.data.forEach(order=>{
                    console.log(order)
                    arrayOfOrders.push({...order , name : order.teacher.name , nameProyector : order.proyector.name , initialHour: moment(order.initialHour).format('HH:mm:ss').toString() ,finalHour: moment(order.finalHour).format('HH:mm:ss').toString() })
                })

                this.setState({
                    data : arrayOfOrders
                })
            }
        })
    }


    render(){
        const { classes } = this.props;
        return(
            <div className={"container-fluid"}>
                    <div className="row w-100 mt-2">
                        <div className="col">
                            <AppBar color="default" className="position-relative">
                                <Toolbar>
                                    <Typography variant="title" color="inherit">
                                        <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                    </Typography>
                                </Toolbar>
                            </AppBar>
                        </div>
                    </div>
                    <div className="container-fluid mt-3">
                        <div className="row">
                            <div className="col">
                                <div name="name" className="entrada-busqueda entrada-busqueda-books pt-2 text-center mt-2 w-100">
                                    <h2>{this.state.date}</h2>
                                </div>
                                <div className="filtrer-time">
                                    {/*<RangePicker onChange={onChange}/>*/}
                                </div>
                            </div>

                            <div className="col-1">
                                <div onClick={this.showModal}>
                                    <a><img className="iconoPDF" src={pdf}/></a>
                                </div>
                            </div>
                            <div className="col-3">
                                <Button variant="contained" className="btn-agendar" onClick={() => {
                                    this.props.history.push('/agendar-proyector')
                                }}>
                                    <NavLink className="linkEstilos" to="/agendar-proyector">Agendar</NavLink>
                                </Button>
                            </div>
                        </div>
                            <Modal
                                title="Descargar reporte"
                                visible={this.state.visible}
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                            >
                                <div className="w-100 justify-content-center text-center">
                                    <div className={`w-100 m-2 ${this.state.showErrorPdf ? '' : 'd-none'}`}>
                                        <Alert message={this.state.messageError} type="error" showIcon/>
                                    </div>
                                    <RangePicker onChange={(date, dateString) => {
                                        this.setState({
                                            fromDate: dateString[0],
                                            toDate: dateString[1]
                                        })
                                    }}/>

                                    <Button variant="contained" className="mt-3" onClick={() => {
                                        //TODO SEND REQUEST FOR THE PDF
                                        const {fromDate, toDate} = this.state
                                        if (fromDate && toDate) {
                                            console.log(`http://localhost:8000/orderProyector/reporte/${fromDate}/${toDate}`)
                                            axios.get(`http://localhost:8000/orderProyector/reporte/${fromDate}/${toDate}`).then(response => {
                                                if (response.status === 200) {
                                                    window.open('http://127.0.0.1:8887/api/routes/example.pdf')                                                    
                                                } else {
                                                    this.setState({
                                                        showErrorPdf: true,
                                                        messageError: 'error de conexion'
                                                    })
                                                }
                                            })
                                        } else {
                                            this.setState({
                                                showErrorPdf: true,
                                                messageError: 'Por favor seleccione las fechas'
                                            })
                                        }
                                    }}>
                                        Descargar
                                    </Button>
                                </div>
                            </Modal>
                    </div>
                    <div className="w-100 mt-3">
                        <Table columns={columns} className="estilosTablaBooks" dataSource={this.state.data} bordered/>
                    </div>
                </div>
        )
    }
}

const SimpleModalWrapped = withStyles(styles)(Agenda);

export default SimpleModalWrapped
