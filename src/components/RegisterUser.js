import React , {Component} from 'react'
import {NavLink} from 'react-router-dom'
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import arrow from '../img/arrow.png';
import Icono1 from '../img/Icono1.png'
import Icono2 from '../img/Icono2.png'

const RegisterUser = () => (
	<div className={"container-fluid"}>
        <div className="row w-100 mt-2">
            <div className="col">
                <AppBar color="default" className="position-relative">
                    <Toolbar>
                        <Typography variant="title" color="inherit">
                            <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                        </Typography>
                    </Toolbar>
                </AppBar>
            </div>
        </div>
        <div className="container-fluid">
            <div className="w-100 text-center">
                <h1 className="title-title">Registrar Usario</h1>
            </div>
            <div className="d-flex container-fluid  containerUsers">
                <div className=" text-center tarjeta-user-1">
                    <div className="w-100 text-white tarjeta-title">
                        <NavLink to="/registermaestro" href="#" role="tab" aria-selected="false"><h1
                            className="titulo-tarjeta linkEstilos">Maestros</h1></NavLink>
                    </div>
                    <div className="w-100">
                        <NavLink to="/registermaestro" href="#" role="tab" aria-selected="false">
                            <img src={Icono1} width={400} className="tarjeta-icons"/>
                        </NavLink>
                    </div>
                </div>
                <div className=" text-center card  tarjeta-user-2 float-right">
                    <div className="w-100  text-white tarjeta-title">
                        <NavLink to="/registeralumno" href="#" role="tab" aria-selected="false"><h1
                            className="titulo-tarjeta linkEstilos">Estudiantes</h1></NavLink>
                    </div>
                    <div className="w-100">
                        <NavLink to="/registeralumno" href="#" role="tab" aria-selected="false">
                            <img src={Icono2} width={400} className="tarjeta-icons"/>
                        </NavLink>
                    </div>
                </div>
            </div>
	</div>
</div>
)

export default RegisterUser