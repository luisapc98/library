import React , {Component} from 'react'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import arrow from '../img/arrow.png';
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import Background from '../img/background-proyector.jpg';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import {NavLink} from 'react-router-dom'
import axios from 'axios'
import { DatePicker , Select  , Radio , AutoComplete , Modal} from 'antd';
import { TimePicker } from 'antd';
import moment from 'moment'
const RadioGroup = Radio.Group;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const Option = Select.Option;
const OptionSecond = AutoComplete.Option;

const styles = theme => ({

    container: {
      display: "flex",
      flexWrap: "wrap",
    },
    margin: {
        margin: theme.spacing.unit,
      },
      textField: {
        flexBasis: 200,
      },
  });

class AgendarProyector extends Component {
    constructor(){
        super()
        this.state = {
            dataAvailableToSelect : [],
            studentSelected : null,
            proyectorSelected : {},
            proyectorsAvailable : [],
            dateInitial : moment(Date(), 'HH:mm:ss').toString(),
            dateFinal : moment(Date(), 'HH:mm:ss').toString(),
            place : '',
        }

        this.handleOnSubmitButton = this.handleOnSubmitButton.bind(this)
    }

    componentDidMount(){
        axios.get('http://localhost:8000/teachers').then(response=>{
            let arrayOfTeachers = []
            if(response.status === 200){
                response.data.data.forEach(teacher=>{
                    arrayOfTeachers.push({...teacher})
                })
            }
            this.setState({
                dataAvailableToSelect : arrayOfTeachers
            })
        })

        axios.get('http://localhost:8000/proyectors').then(response=>{
            let arrayOfTeachers = []
            if(response.status === 200){
                this.setState({
                    proyectorsAvailable : response.data.data
                })
            }
        })
    }

    handleOnSubmitButton(e){
        e.preventDefault()
        if(this.state.studentSelected){
            if(this.state.place){
                console.log(this.state)
                console.log(this.state.proyectorSelected.id)
                axios.post('http://localhost:8000/orderProyector' , {
                    proyector : this.state.proyectorSelected.id,
                    teacher : this.state.studentSelected,
                    initialHour : this.state.dateInitial,
                    finalHour : this.state.dateFinal,
                    place : this.state.place,
                }).then(response=>{
                    console.log(response)
                    if(response.status === 200){
                        this.success()
                    }
                }).catch(error=>{
                    console.log(error)
                })
            }else{
                this.setError("Se necesita un lugar")
            }
        }else{
            this.setError("Se necesita un profesor")
        }
    }

    success(){
        Modal.success({
            title: '¡Orden registrado!',
            content: 'Se ha registrado el orden del proyector',
            onOk() {
                window.location.reload()
            },
        });
    }

    setError(message){
        Modal.error({
            title: 'Ocurrio un error',
            content: message,
        });
    }

    render(){
        const children = this.state.dataAvailableToSelect.map((teacher) => {
            return <OptionSecond key={teacher.id}>{teacher.name}</OptionSecond>
        })

        return(
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                <div className="container-fluid h-100">
                    <div className="w-100 background-purple h-100 justify-content-center">
                        <form className={"h-100 text-center p-3"}>
                            <div className="">
                                <p className="title-formProyector">Agendar Proyector</p>
                                <div className="formAlumno">
                                    <FormControl className="hola TextRegisterPrestamo mb-3 mt-3">
                                        <AutoComplete
                                            /* style={{width: "90%", marginTop: "1.0em", marginLeft: "0em", marginRight: "1.0em"}}*/
                                            onSelect={(item) => {
                                                console.log(item)
                                                this.setState({
                                                    studentSelected : item
                                                })
                                            }}
                                            onSearch={(event)=>{
                                                console.log(this.state.data)
                                                let newFilterArray = this.state.dataAvailableToSelect.filter(item=>{
                                                    return item.name.toLowerCase().search(event.toLowerCase()) !== -1;
                                                })

                                                this.setState({
                                                    dataAvailableToSelect : newFilterArray,
                                                })

                                                if(event === ""){
                                                    if(this.state.valueChoose === 0){
                                                        this.setState({
                                                            dataAvailableToSelect:this.state.studentsAvailable,
                                                        })
                                                    }else{
                                                        this.setState({
                                                            dataAvailableToSelect:this.state.teachersAvailable,
                                                        })
                                                    }

                                                }
                                            }}
                                            size="large"
                                            placeholder="Busca al profesor..."
                                        >
                                            {children}
                                        </AutoComplete>
                                    </FormControl>
                                    <FormControl className="holaProyector m-4">
                                        <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                            Salon
                                        </InputLabel>
                                        <Input id="cara" className="hello-entradaregisterProyector" onChange={(e)=>{
                                            this.setState({
                                                place : e.target.value
                                            })
                                        }}/>
                                    </FormControl>
                                    <FormControl className="w-100 m-3">
                                        <Select
                                            value={this.state.proyectorSelected.name}
                                            onChange={(value)=>{
                                                console.log(value)
                                                this.setState({
                                                    proyectorSelected : value
                                                })
                                            }}
                                         className={"w-100"}>
                                            {
                                                this.state.proyectorsAvailable.map(proyector=>{
                                                    return(
                                                        <Option value={proyector}>{proyector.name}</Option>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                    <div className="row">
                                        <div className="col">
                                            <TimePicker defaultValue={moment(Date(), 'HH:mm:ss')} size="large"  onChange={(value)=>{
                                                console.log(value)
                                                this.setState({
                                                    dateInitial : value.toString()
                                                })
                                            }}/>
                                        </div>
                                        <div className="col">
                                            <TimePicker defaultValue={moment(Date(), 'HH:mm:ss')} size="large"  onChange={(value)=>{
                                                this.setState({
                                                    dateFinal : value.toString()
                                                })
                                            }}/>
                                        </div>
                                    </div>
                                    <div className="w-100 text-center mt-5">
                                        <Button variant="contained" className="btn-btn-form" onClick={this.handleOnSubmitButton}>Agregar</Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}



export default AgendarProyector