import React , {Component} from 'react'
import fondoPerfil from '../img/fondo-perfil.jpg';
import imagePerfil from '../img/image-perfil.jpeg'
import iconoInicio from '../img/iconoInicio.png';
import iconoLibro from '../img/iconoBook.png';
import iconoUsuario from '../img/iconoContacto.png';
import iconoHistorial from '../img/iconoHistorial.png';
import iconoAgenda from '../img/iconoAgenda.png';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import Fade from '@material-ui/core/Fade';
import Avatar from '@material-ui/core/Avatar';
import {NavLink} from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import {mailFolderListItems , otherMailFolderListItems} from '../components/tileData.js'

const drawerWidth = 250;


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  appFrame: {
    height: "100%",
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: 'auto',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
  },
  'appBar-left': {
    marginLeft: drawerWidth,
  },
  'appBar-right': {
    marginRight: drawerWidth,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
});


class Sidebar extends React.Component {
    constructor(){
      super()
      this.state = {
         anchor: 'left',
      }
    }

    handleChange = event => {
        this.setState({
          anchor: event.target.value,
        });
      };

    render() {
       const { classes } = this.props;
       const { anchor } = this.state;
    const drawer = (
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor={anchor}
      >
        <div className={classes.toolbar} />
        <Divider />
        <List>{mailFolderListItems}</List>
        <Divider />
        <List>{otherMailFolderListItems}</List>
      </Drawer>
    );

    let before = null;
    let after = null;

    if (anchor === 'left') {
      before = drawer;
    } else {
      after = drawer;
    }


       return (
      <div className={`h-100 ${classes.root}`}>
        <div className={classes.appFrame}>
          {before}
        </div>
      </div>
);
}
}
export default withStyles(styles)(Sidebar);