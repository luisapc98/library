import React , {Component} from 'react'
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.min.css';
import Avatar from '@material-ui/core/Avatar';
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import pdf from '../img/pdf.png';
import arrow from '../img/arrow.png';
import { DatePicker } from 'antd';
import { Checkbox, Row, Col } from 'antd';
import {NavLink} from 'react-router-dom'
import axios from 'axios'
import { Table } from 'antd';
import TextField from '@material-ui/core/TextField';
import { Popconfirm, message , Modal } from 'antd';
import moment from 'moment'

function deleteSucess (){
    let context = this
    Modal.success({
        title: 'Orden eliminado',
        content: '',
        onOk() {
            window.location.reload()
          }, 
      });
}


const columns = [{
    title: 'Nombre',
    dataIndex: 'name',
}, {
    title: 'Cara',
    dataIndex: 'cara',
}, {
    title: 'Niveles',
    dataIndex: 'level',
    },
    {
    title: 'Carrera',
    dataIndex: 'career',
    },
    ,{
        title: 'Funciones',
        dataIndex: 'id',
        key: 'id',
        render: text =>  <Popconfirm title="Estas seguro de eliminar este estante?" onConfirm={()=>{
            axios.delete('http://localhost:8000/estantes/'+ text)
            deleteSucess()
        }} okText="Eliminar" cancelText="No"><a href="#">Delete</a></Popconfirm>,
      },
];



class Estantes extends Component {

    constructor(){
        super()
        this.state = {
            data : [],
            date : '',
        }
    }

    componentDidMount(){
        moment.locale('en')
        let date = moment(Date()).format("LLLL")
        console.log(date)
        this.setState({
            date
        })
        axios.get('http://localhost:8000/estantes').then(response=>{
            let arrayOfEstantes = []
            if(response.status === 200){
                response.data.data.forEach(estante=>{
                    console.log(estante)
                    arrayOfEstantes.push({...estante , career : estante.career.name})
                })

                this.setState({
                    data : arrayOfEstantes
                })
            }
        })
    }

    render(){
        return(
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                <div className="w-100">
                        <div className="d-flex mb-2 mt-3">
                            <div name="name" className="entrada-busqueda entrada-busqueda-books pt-2 text-center mt-2">
                                <h2>{this.state.date}</h2>
                            </div>
                            <Button variant="contained" className="btn-agendar-books text-white size-" onClick={()=>{
                                this.props.history.push('/registerestante')
                            }}>
                                <NavLink className="linkEstilos" to="/registerestante">Agregar</NavLink>
                            </Button>
                        </div>
                </div>


                <div className="w-100">
                    <Table columns={columns} dataSource={this.state.data}  bordered />
                </div>
            </div>
        )
    }
}



export default Estantes