import React , {Component} from 'react'
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import FormHelperText from '@material-ui/core/FormHelperText';
import arrow from '../img/arrow.png';
import axios from 'axios'
import {Select} from "antd"
import {Modal} from "antd/lib/index"
import {NavLink} from 'react-router-dom'

const Option = Select.Option;

class RegisterProyecter extends Component {
    constructor() {
        super()
		this.state = {
        	errorName: false,
			errorModel : false,
			name : '',
            model : '',
		}

		this.handleSubmitForm= this.handleSubmitForm.bind(this)
		this.handleChangeForm= this.handleChangeForm.bind(this)
    }

    handleChangeForm(e){
    	this.setState({[e.target.id] : e.target.value})
	}

	
    success(){
        Modal.success({
            title: 'Proyector registrado!',
            content: 'Se ha registrado exitosamente el proyector',
            onOk() {
                window.location.reload()
            },
        });
    }

	handleSubmitForm(e){
    	e.preventDefault()
		console.log(this.state)
		if(this.state.name){
    		if(this.state.model){
    			axios.post('http://localhost:8000/proyectors' , {
                    name : this.state.name,
                    model : this.state.model
                }).then(response=>{
                    console.log(response)
                    if(response.status === 200){
                        this.success()
                    }
                })
			}else{
                this.setState({
                    errorModel : true
                })
			}
		}else{
    		this.setState({
                errorName : true
			})
		}
	}

    render() {
        return (
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                <div className="w-100 background-purple h-100 mt-3">
                    <div className={"h-100"}>
                        <form className="h-100" onSubmit={this.handleSubmitForm} onChange={this.handleChangeForm}>
                            <p className="title-form">Registrar Proyector</p>
                            <div>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">
                                        Nombre de Proyector</InputLabel>
                                    <Input id="name" className="hello-entradaPrestamo" error={this.state.errorName}/>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input"
                                                className="hello-hiPrestamo">Modelo</InputLabel>
                                    <Input id="model" className="hello-entradaPrestamo" required
                                           error={this.state.errorModel}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Ingrese modelo del
                                        proyector</FormHelperText>
                                </FormControl>
                            </div>
                            <div className={"w-100 text-center mt-3"}>
                                <Button className="btn-btnPrestamosRegisterBooks" type="submit">Registrar</Button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default RegisterProyecter