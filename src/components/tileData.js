
import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import fondoPerfil from '../img/fondo-perfil.jpg';
import imagePerfil from '../img/image-perfil.jpeg'
import HomeIcon from '@material-ui/icons/Home';
import BooksIcon from '@material-ui/icons/LibraryBooks';
import AddBookIcon from '@material-ui/icons/NoteAdd';
import AddEstanteIcon from '@material-ui/icons/PlaylistAdd';
import AddProyector from '@material-ui/icons/AddToQueue';
import AddUsers from '@material-ui/icons/PersonAdd';
import DeleteIcon from '@material-ui/icons/Delete';
import ReportIcon from '@material-ui/icons/Report';
import { Link } from 'react-router-dom'
import RegisterProyecter from "./RegisterProyecter"

export const mailFolderListItems = (

  
  <div className="EstilosNav">

  <div className="container-fluid fondo-perfil">

  <img src={fondoPerfil} className="img-fluid" alt="Responsive image" /> 
  <div>  
  <Avatar className="image-perfil" alt="Adelle Charles" src={imagePerfil} />     
  <div className="datos-usuario">
  <p className="nombre-usuario">Biblioteca CID</p>
  <p className="cargo-usuario">Administrador</p>
  </div> 
  </div> 
  
</div>



  { <Link to="/" > 
      <ListItem button>
        <ListItemIcon>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary="Inicio" />
      </ListItem>
    </Link> } 
    { <Link to="/historialprestamos" > 
    <ListItem button>
      <ListItemIcon>
        <BooksIcon />
      </ListItemIcon>
      <ListItemText primary="Agendar Libro" />
    </ListItem>
  </Link> } 
  { <Link to="/books" > 
      <ListItem button>
        <ListItemIcon>
          <AddBookIcon />
        </ListItemIcon>
        <ListItemText primary="Registrar Libro" />
      </ListItem>
    </Link> } 
    { <Link to="registerestante" > 
    <ListItem button>
      <ListItemIcon>
        <AddEstanteIcon />
      </ListItemIcon>
      <ListItemText primary="Registrar Estante" />
    </ListItem>
  </Link> }
      { <Link to="estantes" >
          <ListItem button>
              <ListItemIcon>
                  <AddEstanteIcon />
              </ListItemIcon>
              <ListItemText primary="Estantes" />
          </ListItem>
      </Link> }
    { <Link to="agenda" > 
      <ListItem button>
        <ListItemIcon>
          <AddProyector />
        </ListItemIcon>
        <ListItemText primary="Agendar Proyector" />
      </ListItem>
    </Link> }
    { <Link to="RegisterProyecter" >
      <ListItem button>
        <ListItemIcon>
          <AddProyector />
        </ListItemIcon>
        <ListItemText primary="Registrar Proyector" />
      </ListItem>
    </Link> }
  </div>
);

export const otherMailFolderListItems = (
  <div className="EstilosNav">
  { <Link to="registrar">
      <ListItem button>
        <ListItemIcon>
          <AddUsers />
        </ListItemIcon>
        <ListItemText primary="Registrar Usuario" />
      </ListItem>
    </Link> } 
  </div>
);
