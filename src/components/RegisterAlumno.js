import React , {Component} from 'react'
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import arrow from '../img/arrow.png';
import axios from "axios/index"
import {Select,Modal} from 'antd';
import {NavLink} from 'react-router-dom'


const Option = Select.Option;

class RegisterAlumno extends Component {
    constructor() {
        super()
		this.state = {
            name : "",
            curp : "",
            phoneNumber : null,
            itemSelected : {},
            errorName : false,
            errorNumber : false,
            errorCurp : false,
            careers : [],
		}

        this.handleSubmitForm= this.handleSubmitForm.bind(this)
        this.handleChangeForm= this.handleChangeForm.bind(this)
    }


    success(){
        Modal.success({
            title: '¡Alumno registrado!',
            content: 'Se ha registrado exitosamente al alumno',
            onOk() {
                window.location.reload()
            },
        });
    }

    componentDidMount(){
        axios.get('http://localhost:8000/careers').then(response=>{
            console.log(response.data.data)
            this.setState({
                careers : response.data.data
            })
        }).catch(error=>{
            console.log(error)
        })
	}

    handleSubmitForm(e){
    	e.preventDefault()
        if(this.state.name){
            if(this.state.curp){
                if(this.state.phoneNumber){
                	if(this.state.itemSelected.id){
                        axios.post('http://localhost:8000/students' , {
                            name : this.state.name,
                            matricula : this.state.curp,
                            phoneNumber : this.state.phoneNumber,
                            career : this.state.itemSelected.id
                        }).then((response)=>{
                            if(response.status === 200){
                                this.success()
                            }
                        }).catch(error=>{
                        	console.log(error)
                        })
					}
                }else {
                    this.setState({
                        errorNumber : true
                    })
                }
            }else {
                this.setState({
                    errorCurp : true
                })
            }
        }else{
            this.setState({
                errorName : true
            })
        }
	}

    handleChangeForm(e){
        e.preventDefault()
        this.setState({[e.target.id] : e.target.value})
    }


    render() {
        return (
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>

                <div className="w-100 background-purple h-100 justify-content-center">
                    <form className="h-100 text-center" onChange={this.handleChangeForm} onSubmit={this.handleSubmitForm}>
                            <div className="form-login form-loginAlumno">
                                <p className="title-form title-formAlumno">Registro Alumno</p>
                                <div className="formAlumno">
                                    <FormControl className="hola">
                                        <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                            Nombre
                                        </InputLabel>
                                        <Input id="name" className="hello-entradaregisterAlumno"/>
                                    </FormControl>

                                    <FormControl className="hola">
                                        <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                            Numero de telefono
                                        </InputLabel>
                                        <Input id="phoneNumber" className="hello-entradaregisterAlumno" type={"number"}/>
                                    </FormControl>

                                    <FormControl className="hola">
                                        <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                            Matricula
                                        </InputLabel>
                                        <Input id="curp" className="hello-entradaregisterAlumno"/>
                                    </FormControl>
                                    <FormControl className="hola">
                                        <Select
                                            value={this.state.itemSelected.name}
                                            onChange={(value)=>{
                                                console.log(value)
                                                this.setState({
                                                    itemSelected : value
                                                })
                                            }}
                                        >
                                            {
                                                this.state.careers.map(career=>{
                                                    return(
                                                        <Option value={career}>{career.name}</Option>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>

                                </div>
                                <div className="w-100 text-center">
                                    <Button variant="contained" className="btn-btn-form" type={"submit"}>
                                        Registrar
                                    </Button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default RegisterAlumno