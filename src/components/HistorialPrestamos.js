import React, {Component} from 'react'
import classNames from 'classnames'
import 'bootstrap/dist/css/bootstrap.min.css'
import Avatar from '@material-ui/core/Avatar'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import pdf from '../img/pdf.png'
import arrow from '../img/arrow.png'
import {DatePicker} from 'antd'
import {Checkbox, Row, Col} from 'antd'
import Icono3 from '../img/Icono3.png'
import Icono4 from '../img/Icono4.png'
import { Table } from 'antd';
import axios from 'axios'
import {NavLink} from 'react-router-dom'
import {withRouter} from 'react-router-dom'
import { Popconfirm, message , Modal  , Alert} from 'antd';
import moment from 'moment'

function deleteSucess (){
    let context = this
    Modal.success({
        title: 'Estante eliminado',
        content: '',
        onOk() {
            window.location.reload()
          }, 
      });
}
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const columns = [{
    title: 'Nombre',
    dataIndex: 'name',
}, {
    title: 'Libro',
    dataIndex: 'bookName',
},
    {
        title: 'Tipo',
        dataIndex: 'type',
    },
    {
        title: 'Fecha inicial',
        dataIndex: 'date_from',
    },
    {
        title: 'Fecha de entrega',
        dataIndex: 'date_final',
    },
    {
        title: 'Status',
        dataIndex: 'status',
    },{
        title: 'Funciones',
        dataIndex: 'id',
        key: 'id',
        render: text =>  <Popconfirm title="Estas seguro de eliminar este orden?" onConfirm={()=>{
            axios.delete('http://localhost:8000/orderBooks/'+ text)
            deleteSucess()
        }} okText="Eliminar" cancelText="No"><a href="#">Delete</a></Popconfirm>,
      },
]



class HistorialPrestamos extends Component {

    constructor(){
        super()
        this.state = {
            data : [],
            date : '',
            visible: false,
            fromDate : '',
            toDate : '',
            showErrorPdf : false,
            messageError : '',
            studentCount : 0,
            teacherCount : 0,
        }
    }

    showModal = () => {
        console.log("Clicking button")
        this.setState({
          visible: true,
        });
      }
  
      handleOk = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
      }
  
      handleCancel = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
      }
  

    componentDidMount(){
        moment.locale('en')
        let date = moment(Date()).format("LLLL")
        console.log(date)
        this.setState({
            date
        })

        axios.get('http://localhost:8000/orderBooks').then((response)=>{
            let arrayOfPeople = []
            let studentCount = 0;
            let teacherCount= 0;
            if(response.status === 200){
                console.log(response.data.data)
                response.data.data.forEach(order=>{
                    console.log(order)
                    let dateFromFormatedFrom = moment(order.date_from).format("MM-DD-YYYY")
                    let dateFromFormatedFinal = moment(order.date_final).format("MM-DD-YYYY")
                    let name = "";
                    let type = "";
                    if(order.teacher === null){
                        name = order.student.name
                        type = "Estudiante"
                        studentCount = studentCount + 1
                    }else if(order.student === null){
                        name = order.teacher.name
                        type = "Profesor"
                        teacherCount = teacherCount +1
                    }
                    arrayOfPeople.push({...order , bookName : order.book.title , name , type , date_from : dateFromFormatedFrom , date_final : dateFromFormatedFinal})
                })
                this.setState({
                    data : arrayOfPeople,
                    teacherCount,
                    studentCount
                })
            }
        })
    }

    render(){
        return(
            <div className="container-fluid">
                <Modal
                                title="Descargar reporte"
                                visible={this.state.visible}
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                            >
                                <div className="w-100 justify-content-center text-center">
                                    <div className={`w-100 m-2 ${this.state.showErrorPdf ? '' : 'd-none'}`}>
                                        <Alert message={this.state.messageError} type="error" showIcon/>
                                    </div>
                                    <RangePicker onChange={(date, dateString) => {
                                        this.setState({
                                            fromDate: dateString[0],
                                            toDate: dateString[1]
                                        })
                                    }}/>

                                    <Button variant="contained" className="mt-3" onClick={() => {
                                        //TODO SEND REQUEST FOR THE PDF
                                        const {fromDate, toDate} = this.state
                                        if (fromDate && toDate) {
                                            console.log(`http://localhost:8000/orderBooks/reporte/${fromDate}/${toDate}`)
                                            axios.get(`http://localhost:8000/orderBooks/reporte/${fromDate}/${toDate}`).then(response => {
                                                if (response.status === 200) {
                                                    window.open('http://127.0.0.1:8887/api/routes/example.pdf')                                                    
                                                } else {
                                                    this.setState({
                                                        showErrorPdf: true,
                                                        messageError: 'error de conexion'
                                                    })
                                                }
                                            })
                                        } else {
                                            this.setState({
                                                showErrorPdf: true,
                                                messageError: 'Por favor seleccione las fechas'
                                            })
                                        }
                                    }}>
                                        Descargar
                                    </Button>
                                </div>
                </Modal>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                <div className="container-fluid mt-3">
                        <div className="row">
                            <div className="col">
                                <div name="name" className="entrada-busqueda entrada-busqueda-books pt-2 text-center mt-2 w-100">
                                    <h2>{this.state.date}</h2>
                                </div>
                                <div className="filtrer-time">
                                    {/*<RangePicker onChange={onChange}/>*/}
                                </div>
                            </div>
                            <div className="col-1">
                                <div onClick={this.showModal}>
                                    <a><img className="iconoPDF" src={pdf}/></a>
                                </div>
                          </div>
                          <div className="col-3">

                            <Button variant="contained" className="btn-agendar-booksPrestamos text-white size-" onClick={()=>{
                                this.props.history.push('/registerprestamo')
                            }}>
                                <NavLink to="/registerprestamo" className="linkEstilos">Agendar
                                </NavLink>
                            </Button>
                    </div>
                    </div>

                    
                </div>
                <div>
                    <div className="row w-100 mt-2">
                        <div className="col">
                            <Table columns={columns} bordered dataSource={this.state.data}/>
                        </div>
                        <div className="col-md-4">
                            <div className={"w-100 justify-content-right"}>
                                <div className="text-center card">
                                    <div className="w-100">
                                        <h1 className="titulo-tarjetaUno">Prestamos Alumnos</h1>
                                    </div>
                                    <div className="w-100">
                                        <img src={Icono3} width={700} className="tarjeta-iconsPrestamo"/>
                                        <h1>{this.state.studentCount}</h1>
                                    </div>
                                </div>
                                <div className=" text-center card">
                                    <div className="w-100">
                                        <h1 className="titulo-tarjetaUno">Prestamos Maestros</h1>
                                    </div>
                                    <div className="w-100">
                                        <img src={Icono4} width={700} className="tarjeta-iconsPrestamoDos"/>
                                        <h1>{this.state.teacherCount}</h1>
                                    </div>
                                </div>
                                <div className=" text-center card ">
                                    <div className="w-100">
                                        <h1 className="titulo-tarjetaDos">Total Prestados</h1>
                                        <h1>{this.state.studentCount + this.state.teacherCount}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



export default withRouter(HistorialPrestamos)