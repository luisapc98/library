import React , {Component} from 'react'
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import FormHelperText from '@material-ui/core/FormHelperText';
import arrow from '../img/arrow.png';
import axios from 'axios'
import {Select} from "antd"
import {Modal} from "antd/lib/index"
import {NavLink} from 'react-router-dom'
const Option = Select.Option;

class RegisterBooks extends Component {
    constructor() {
        super()
		this.state = {
        	errorTitle: false,
            errorPublisher: false,
			errorNumOfEdition : false,
			errorAuthor : false,
            errorBookcase: false,
            errorQuantity : false,
			title : '',
			publisher : '',
			numOfEdition : '',
            itemSelected : {},
			author: '',
            bookcase : '',
			yearOfPublish : 0,
			level : 0,
            estantesAvailable : [],
            quantity : 1,
		}

		this.handleSubmitForm= this.handleSubmitForm.bind(this)
		this.handleChangeForm= this.handleChangeForm.bind(this)
		this.cleanView= this.cleanView.bind(this)
    }

    handleChangeForm(e){
    	this.setState({[e.target.id] : e.target.value})
	}


	componentDidMount(){
        axios.get('http://localhost:8000/estantes').then((response)=>{
            if(response.status === 200){
                console.log(console.response)
                this.setState({
                    estantesAvailable : response.data.data
                })
            }
        }).catch(error=>{
            console.log(error)
        })
	}

    success(){
        let context = this
   
        Modal.success({
            title: '¡Libro registrado!',
            content: 'Se ha registrado exitosamente el Libro',
            onOk() {
                context.cleanView()
            },
        });
    }


    cleanView(){
      this.setState({
        title : '',
        publisher : '',
        numOfEdition : '',
        itemSelected : {},
        author: '',
        bookcase : '',
        yearOfPublish : 0,
        level : 0,
        quantity : 1,
      })
    }

    error() {
      Modal.error({
        title: 'Necesitas selecionar el estante',
        content: 'Selecciona un estante',
      });
    }

	handleSubmitForm(e){
    	e.preventDefault()
		console.log(this.state)
		if(this.state.title){
    		if(this.state.publisher){
    			if(this.state.numOfEdition){
    				if(this.state.author){
                        if(this.state.quantity){
                            if(this.state.itemSelected.name){
                            axios.post(`http://localhost:8000/books` , {
                                title : this.state.title,
                                publisher : this.state.publisher,
                                num_of_edicion : this.state.numOfEdition,
                                author : this.state.author,
                                estante : this.state.itemSelected.id,
                                level : this.state.level,
                                quantity : this.state.quantity
                            }).then(response=>{
                                if(response.status === 200){
                                    this.success()
                                }
                            }).catch(error=>{
                                    console.log(error)
                                })
                            }else{
                                //TODO : SHOW MODAL WINDOW OF ERROR
                                this.error()
                            }
                        }else{
                            this.setState({
                                errorQuantity : true
                            })
                        }
					}else{
    					this.setState({
                            errorAuthor : true
						})
					}
				}else{
    				this.setState({
                        errorNumOfEdition : true
					})
				}
			}else{
                this.setState({
                    errorPublisher : true
                })
			}
		}else{
    		this.setState({
                errorTitle : true
			})
		}
	}

    render() {
        return (
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                    <div className="w-100 background-purple h-100">
                        <form className={"h-100"} onSubmit={this.handleSubmitForm} onChange={this.handleChangeForm}>
                            <p className="title-form title-formPrestamoBooks">Registro Libros</p>
                            <div className="">
                                <FormControl className="holaPrestamo" name={"level"}>
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">Nivel de estante</InputLabel>
                                    <Input id="level" className="hello-entradaPrestamo" defaultValue={0} type={"number"} value={this.state.level}/>
								</FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">Editorial</InputLabel>
                                    <Input id="publisher" className="hello-entradaPrestamo" required error={this.state.errorPublisher} value={this.state.publisher}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Inserte un editorial</FormHelperText>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">
                                        Nombre del Libro
                                    </InputLabel>
                                    <Input id="title" className="hello-entradaPrestamo" required error={this.state.errorTitle} value={this.state.title}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Inserte un editorial</FormHelperText>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">
                                        Numero de edición
                                    </InputLabel>
                                    <Input id="numOfEdition" className="hello-entradaPrestamo" type={"number"} error={this.state.errorNumOfEdition} value={this.state.numOfEdition}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Inserte un editorial</FormHelperText>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <Select
                                        value={this.state.itemSelected.name}
                                        className={"w-75"}
                                        onChange={(value)=>{
                                            console.log(value)
                                            this.setState({itemSelected : value})
                                        }}
                                    >
                                        {
                                            this.state.estantesAvailable.map(estante=>{
                                                return(
                                                    <Option value={estante}>{estante.name}</Option>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">
                                        Año de Publicación
                                    </InputLabel>
                                    <Input id="yearOfPublish" className="hello-entradaPrestamo" type={"number"} value={this.state.yearOfPublish}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Inserte un editorial</FormHelperText>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">
                                        Cantidad
                                    </InputLabel>
                                    <Input id="quantity" className="hello-entradaPrestamo" type={"number"} defaultValue={1} value={this.state.quantity}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Inserte un numero</FormHelperText>
                                </FormControl>
                                <FormControl className="holaPrestamo">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo">Autor</InputLabel>
                                    <Input id="author" className="hello-entradaPrestamo" error={this.state.errorAuthor} error={this.state.errorQuantity} value={this.state.author}/>
                                    <FormHelperText className="hello-nameError" id="name-error-text">Inserte un editorial</FormHelperText>
                                </FormControl>
                                <div className={"w-100 text-center"}>
                                    <Button className="btn-btnPrestamosRegisterBooks mt-2" type="submit">Registrar</Button>
                                </div>
                                <div className="d-flex containerDOS">
                                    <div className="FooterTrianguloLeft">

                                    </div>
                                    <div className="FooterTrianguloRight">

                                    </div>
                                </div>
                            </div>
                        </form>

                </div>
            </div>
        )
    }
}

export default RegisterBooks