import React, { Component } from 'react';
import './App.css';
import {NavLink  , BrowserRouter as Router , Route , Switch , Redirect} from 'react-router-dom'
import Sidebar from './components/sidebar'
import Home from './components/Home'
import Agenda from './components/Agenda'
import Books from './components/Books'
import Estantes from './components/Estantes'
import RegisterUser from './components/RegisterUser'
import RegisterAlumno from './components/RegisterAlumno'
import Prestamos from './components/Prestamos'
import Dashboard from "./pages/Dashboard"
import Login from "./pages/Login"
import formularioAgendarProyector from './components/AgendarProyector'
import RegisterBooks from './components/RegisterBooks'
import HistorialPretamos from './components/HistorialPrestamos'
import axios from 'axios'
import RegisterProyector from "./components/RegisterProyecter"


const carrer = [
    {
        name : "Otros"
    },
]


class App extends Component {
    constructor() {
        super()
    }


    componentDidMount() {
        //s carrer.map(name=>{
        //     axios.post('http://localhost:8000/careers' , {
        //       name : name.name
        //     }).then(reponse=>{
        //       console.log(reponse)
        //     }).catch(error=>{
        //       console.log(error)
        //     })
        // })
    }


    render() {
        return (
                <div className="container-fluid h-100">
                    <Router>
                        <Switch>
                            <Route component={Dashboard} exact path="/"/>
                            <Route component={Login} path="/login"/>
                            <Route render={()=>{
                              return(
                                  <Redirect to={"/"}/>
                              )
                            }}/>
                        </Switch>
                    </Router>
                </div>
            
        )
    }
}


export default App
