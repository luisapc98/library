import React , {Component} from 'react'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import Background from '../img/background-home.jpg';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';


const styles = theme => ({

    container: {
      display: "flex",
      flexWrap: "wrap",
    },
    margin: {
        margin: theme.spacing.unit,
      },
      textField: {
        flexBasis: 200,
      },
  });

class Login extends Component {
    constructor(){
        super()
    }


    handleMouseDownPassword = event => {
        event.preventDefault();
      };
    
      handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
      };


    render(){
        return(
         <div className="">
            <div className="">
                <img src={Background} alt="Background Login" className="fondo-login"/>
            </div>

            <div className="contenedor-login">
                <div className="card-login">
                <div className="form-login">
                    <p className="title-form">Inicio de Sesión</p>
                    <FormControl className="">
                    <InputLabel htmlFor="custom-css-input">
                        Usuario
                    </InputLabel>
                    <Input id="custom-css-input"/>
                    </FormControl>

                    <FormControl className={classNames("classes.margin", "classes.textField")}>
                    <InputLabel htmlFor="adornment-password">Contraseña</InputLabel>
                    <Input id="adornment-password"/>
                  </FormControl>
                  <div className="btn-form">
                  <Button variant="contained" className="btn-btn-form">
                  Iniciar
                </Button>
                <div className="triangle-down-left"></div>
                </div>
              </div>
             </div>
            </div>

        </div>
        )
    }
}



const LoginUser = withStyles(styles)(Login);
export default LoginUser