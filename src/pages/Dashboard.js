import React, { Component } from 'react';
import '../App';
import {NavLink  , BrowserRouter as Router , Route , Switch , Redirect} from 'react-router-dom'
import Sidebar from '../components/sidebar'
import Home from '../components/Home'
import Agenda from '../components/Agenda'
import Books from '../components/Books'
import Estantes from '../components/Estantes'
import RegisterUser from '../components/RegisterUser'
import RegisterAlumno from '../components/RegisterAlumno'
import RegisterMaestro from '../components/RegisterMaestro'
import RegisterEstante from '../components/RegisterEstante'
import Prestamos from '../components/Prestamos'
import RegisterPrestamo from '../components/RegisterPrestamo'
import RegisterBooks from '../components/RegisterBooks'
import HistorialPrestamos from '../components/HistorialPrestamos'
import FormularioAgendarProyector from "../components/AgendarProyector"
import RegisterProyecter from "../components/RegisterProyecter"

class Dashboard extends Component {
  render() {
    return (
    <div className="h-100">
        <Router>
                <div className="container-fluid h-100 d-flex">
                <Sidebar/>
                <Switch>
                    <Route component = {Home} exact path="/"/>
                    <Route component = {Agenda} path="/Agenda"/>
                    <Route component = {Books} path="/books"/>
                    <Route component = {Estantes} path="/estantes"/>
                    <Route component = {RegisterUser} path="/registrar"/>
                    <Route component = {RegisterAlumno} path="/registeralumno"/>
                    <Route component = {RegisterMaestro} path="/registermaestro"/>
                    <Route component = {RegisterEstante} path="/registerestante"/>
                    <Route component = {RegisterPrestamo} path="/registerprestamo"/>
                    <Route component = {RegisterBooks} path="/registerbooks"/>
                    <Route component = {RegisterProyecter} path="/registerproyecter"/>
                    <Route component = {Prestamos} path="/prestamos"/>
                    <Route component = {HistorialPrestamos} path="/historialprestamos"/>
                    <Route component = {FormularioAgendarProyector} path="/agendar-proyector"/>
                    <Route render={()=> <Redirect to={"/"}/>}/>
                </Switch>
                </div>
        </Router>
      </div>
    );
  }
}

export default Dashboard;
